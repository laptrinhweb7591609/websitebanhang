using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using WebsitePhucLong.Models;
using WebsitePhucLong.ViewModels;

namespace WebsitePhucLong.Controllers
{
    public class HomeController : Controller
    {
        private readonly WebsiteBanHangContext _context;

        public HomeController(WebsiteBanHangContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var menus = await _context.Menus.Where(m => m.Hide == 0).OrderBy(m => m.Order).ToListAsync();
            var blogs = await _context.Blogs.Where(m => m.Hide == 0).OrderBy(m => m.Order).Take(2).ToListAsync();
            var slides = await _context.Sliders.Where(m => m.Hide == 0).OrderBy(m => m.Order).ToListAsync();
            var iceblended_prods = await _context.Products.Where(m => m.Hide == 0 && m.IdCat == 2).OrderBy(m => m.Order).Take(3).ToListAsync();
            var tea_prods = await _context.Products.Where(m => m.Hide == 0 && m.IdCat == 1).OrderBy(m => m.Order).Take(3).ToListAsync();


            var viewModel = new HomeViewModel
            {
                Menus = menus,
                Blogs = blogs,
                Sliders = slides,
                IceBlendedProds = iceblended_prods,
                TeaProds = tea_prods,
            };

            return View(viewModel);
        }


        public async Task<IActionResult> _SlidePartial()
        {
            return PartialView();
        }
        public async Task<IActionResult> _ProductPartial()
        {
            return PartialView();
        }

        public async Task<IActionResult> _MenuPartial()
        {
            return PartialView();
        }

        public async Task<IActionResult> _BlogPartial()
        {
            return PartialView();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
