﻿using WebsitePhucLong.Models; 

namespace WebsitePhucLong.ViewModels
{
    public class HomeViewModel
    {
        public List<Menu> Menus { get; set; }
        public List<Blog> Blogs { get; set; }
        public List<Slider> Sliders { get; set; }
        public List<Product> TeaProds { get; set; }
        public List<Product> IceBlendedProds { get; set; }
    }
}
